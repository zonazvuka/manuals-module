<?php
Yii::import('application.modules.manuals.models.Manuals');

class manualsBehavior extends CActiveRecordBehavior
{

    public function beforeSave($event)
    {
        $this->_filesHandler();
        return parent::beforeSave($event);
    }

    public function beforeDelete($event)
    {
        Manuals::model()->deleteAllByAttributes(array('product_id'=>$this->owner->id));
        return parent::beforeDelete($event);
    }

    protected function _filesHandler()
    {
        if (isset($_POST['Manuals'])) {
            foreach ($this->owner->manuals as $manual) {
                $manual->save();
            }
        }
    }

}
