<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of deleteAction
 *
 * @author strong
 */
class deleteAction extends CAction {

    public function run($id) {
        $manual = Manuals::model()->findByPk($id);
        if (is_null($manual)) {
            echo CJSON::encode(array(
                'status' => false,
            ));
            Yii::app()->end();
        }
        $fileName = Yii::app()->basePath . '/../files/manuals/' . $manual->name;
        if (file_exists($fileName)) {
            unlink($fileName);
        }
        if ($manual->delete()) {
            echo CJSON::encode(array(
                'status' => true,
            ));
            Yii::app()->end();
        }
        echo CJSON::encode(array(
            'status' => false,
        ));
    }

}
