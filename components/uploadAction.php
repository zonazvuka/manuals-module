<?php

Yii::import('application.modules.manuals.models.Manuals');

class uploadAction extends CAction {

    public $manualsFieldName = 'manualsField';

    public function run($ownerId) {
        $owner = $this->getController()->loadModel($ownerId);
        $manual = new Manuals(Manuals::SCENARIO_UPLOAD);
        if (isset($_FILES['Manuals'])) {
            $manual->manualsField = $_FILES['Manuals']['name'];
            $manual->manualsField = CUploadedFile::getInstance($manual, $this->manualsFieldName);
            $manual->product_id = $owner->id;
            $baseUrl = Yii::getPathOfAlias('webroot');
            if ($manual->save()) {
                $name = $owner->brand->url . '-' . $owner->url . '--' . $manual->id . '.pdf';
                $manual->name = $name;
                $manual->save();
                $manual->manualsField->saveAs($baseUrl . '/files/manuals/' . $name);
                echo CJSON::encode(array('filename' => $manual->manualsField->name, 'manualId' => $manual->id));
            }
        }
    }

}
