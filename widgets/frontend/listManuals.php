<?php

class listManuals extends CWidget
{
    public $productModel;
    public $relationName = 'manuals';
    public $first = false;
    protected $_manuals = array();

    public function init()
    {
        if (!is_null($this->productModel)) {
            $this->_manuals = $this->productModel->{$this->relationName};
        }
    }

    public function run()
    {
        $this->render('list', array(
            'dataProvider'=>new CArrayDataProvider($this->_manuals, array(
                'pagination'=>false,
             )),
        ));
    }

}
