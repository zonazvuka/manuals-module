<?php

Yii::import('application.modules.manuals.models.Manuals');

class manualsFormWidget extends CInputWidget {

    public $model;
    public $attribute;
    public $submitSelector = '#submit';

    public function init() {
        $this->_registerScript();
    }

    public function run() {
        if (is_null($this->model)) {
            return;
        }

        if ($this->model->scenario == 'update') {
            $box = $this->_getUploadedFilesBox();
            echo CHtml::tag('div', array('class' => 'infobox-widget', 'id' => 'info_' . $this->getId()), '<label></label><span></span>');
            echo CHtml::tag('div', array('class' => 'filelistbox-widget', 'id' => 'filelist_' . $this->getId()), $box);
            echo CHtml::openTag('div', array('class' => 'row'));
            echo CHtml::label('', false);
            echo CHtml::fileField($this->_fieldName());
            echo CHtml::closeTag('div');
        } else {
            echo "Загрузка файлов доступна только после добавления товара.";
        }
    }

    protected function _registerScript() {
        $baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('manuals.widgets.backend.assets'));
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/jquery.ajaxfileupload.js', CClientScript::POS_END);
        $cs->registerScript('uploadFile_' . $this->getController()->getId() . $this->getId(), "
            var manualsSubmitValue = $('" . $this->submitSelector . "').val();
            $('input[name=\"" . $this->_fieldName() . "\"]').ajaxfileupload({
                'action': '" . $this->_uploadActionUrl() . "',
                'valid_extensions': ['pdf'],
                'params': {
                  'extra': 'info'
                },
                'onComplete': function(response) {
                    $('#info_" . $this->getId() . " span').text('');
                    if (response == '') {
                        $('#info_" . $this->getId() . " span').text('Невозможно загрузить файл');
                    } else {
                        $('#filelist_" . $this->getId() . "').prepend(manualItem(response));
                    }
                    $('" . $this->submitSelector . "').prop('disabled', false);
                    $('" . $this->submitSelector . "').val(manualsSubmitValue);
                    $(this).val('');
                },
                'onStart': function() {
                  $('#info_" . $this->getId() . " span').text('Loading...');
                  $('" . $this->submitSelector . "').prop('disabled', true);
                  $('" . $this->submitSelector . "').val('Идет загрузка файла...');
                },
                'onCancel': function() {
                    $('" . $this->submitSelector . "').prop('disabled', false);
                    $('" . $this->submitSelector . "').val(manualsSubmitValue);
                }
            });
            
            $('#filelist_" . $this->getId() . "').on('click', '.delete_manual_button', function() {
                if(confirm('Вы действительно хотите удалить этот элемент?')) {
                    var id = $(this).attr('data-id');
                    $.ajax({
                        'complete': $(this).parent().hide(\"slow\"),
                        'url':'/catalog/products/deleteManual?id=' + id,
                        'cache':false
                    });
                }
            });

            function manualItem(response) {
                if (typeof response.filename==='undefined') {
                    $('#info_" . $this->getId() . " span').text(response.message);
                    return;
                }
                return '<div class=\"row\"><label></label>' + response.filename + ' <input class=\"delete_manual_button\" data-id=\"' + response.manualId + '\" type=\"button\" value=\"удалить\"></div>';
            }
    ");
    }

    protected function _getUploadedFilesBox() {
        $box = '';
        foreach ($this->model->manuals as $manual) {
            $box .= $this->_uploadedFileTemplate($manual);
        }
        return $box;
    }

    protected function _uploadedFileTemplate($model) {
        return '<div class="row" id="manual_' . $model->id . '">'
                . '<label></label>'
                . $model->name . ' '
                . CHtml::ajaxButton('удалить', array('deleteManual', 'id' => $model->id), array('complete' => 'js: $("#manual_' . $model->id . '").hide("slow")'), array('confirm' => 'Вы действительно хотите удалить этот элемент?'))
                . '</div>';
    }

    protected function _fieldName($attr = null, $id = null) {
        return ('Manuals[' . (is_null($attr) ? $this->attribute : $attr) . ']' . (!is_null($id) ? '[' . $id . ']' : ''));
    }

    protected function _uploadActionUrl() {
        $controller = $this->getController();
        return $controller->createUrl($controller->getId() . '/uploadManual', array('ownerId' => $this->model->id));
    }

}
